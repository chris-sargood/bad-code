﻿using System;

namespace ThisCodeSucks
{
    class Program
    {
        static void Main(string[] args)
        {
            var messyClass = new Class1();
            var result = messyClass.Calculate(500, 199, 19);
            Console.WriteLine(result);
        }
    }
}